Ronald Locke / Germany / Berlin

I made these Live Templates for PhpStorm. My favorite PHP IDE. These guys rock who developed this IDE.

There are Drupal 6 and Drupal 7 Live Templates in this repository.

Because I develop D6 and D7 side by side I had to choose a way to access them both. I decided to go with:

hook6_
hook7_
fapi6_
fapi7_
schema6_
schema7_
drupal6_
drupal7_

and so forth. This will continue that way.

** NOTES **

There are many files at the moment for Drupal 7, because I do use a parser to get the data. 

This set of live templates is far from finished. I update as I use them.